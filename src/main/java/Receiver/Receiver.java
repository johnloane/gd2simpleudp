package Receiver;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class Receiver {
    public static void main(String[] args) {
        int receiverPort = Integer.parseInt("50001");
        int senderPort = Integer.parseInt("50000");

        boolean continueRunning = true;

        final int MAX_LEN = 50;

        DatagramSocket receiverSocket = null;

        try{
            //Create a datagram socket for receiving data
            receiverSocket = new DatagramSocket(receiverPort);

            System.out.println("Waiting for a message on port " + receiverPort + " .....");
            while(continueRunning){
                byte buffer[] = new byte[MAX_LEN];
                DatagramPacket receiverPacket = new DatagramPacket(buffer, MAX_LEN);
                receiverSocket.receive(receiverPacket);
                //Convert the buffer of data into a String
                String message = new String(buffer);

                System.out.println("Message received : " + message);

                //Figure out where the message came from
                InetAddress sender = receiverPacket.getAddress();
                System.out.println("Sender: " + sender);

                //Send a message back to the sender
                String responseMessage = "Thank you";
                byte[] responseArray = responseMessage.getBytes();
                DatagramPacket responsePacket = new DatagramPacket(responseArray, responseArray.length, sender, senderPort);
                //Send the packet
                receiverSocket.send(responsePacket);
            }
        }catch(SocketException se){
            se.printStackTrace();
        }catch(IOException io){
            io.printStackTrace();
        }finally{
            if(receiverSocket != null){
                receiverSocket.close();
            }
        }

    }


}
