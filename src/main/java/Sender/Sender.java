package Sender;

import java.io.IOException;
import java.net.*;

public class Sender {
    public static void main(String[] args) {
        //Set up the socket object so that it can closed in the
        // finally block
        DatagramSocket senderSocket = null;
        try{
            //Set up the address information
            InetAddress receiver =
                    InetAddress.getByName("10.102.21.253");

            //Create the port for the sender to listen on
            int senderPort = Integer.parseInt("50000");
            int receiverPort = Integer.parseInt("50001");

            //Message to send
            String message = "Hello from John";

            //Set up a DatagramSocket to send data
            //Make sure to set the port to the listening port
            senderSocket = new DatagramSocket(senderPort);

            //Get the information to be sent as a byte array
            byte buffer[] = message.getBytes();

            //Build the datagram packet
            //REQUIRES:
            //buffer: Data to be sent as a byte array
            //buffer.length: Length of data to send
            //receiver: IP address of the computer to send to
            //receiverPort: Port of the receiver

            DatagramPacket datagram = new DatagramPacket(buffer,
                    buffer.length, receiver, receiverPort);

            //Send the message
            senderSocket.send(datagram);
            System.out.println("Message sent");

            //Make a datagram packet to receive the response
            byte[] response = new byte[15];
            DatagramPacket reply = new DatagramPacket(response,
                    response.length);

            //Receive the response
            senderSocket.receive(reply);

            //Get the content of the response
            String responseMessage = new String(reply.getData());
            System.out.println("Response: " + responseMessage);
        }catch(UnknownHostException ue){
            ue.printStackTrace();
        }
        catch(SocketException se){
            se.printStackTrace();
        }
        catch( IOException e){
            e.printStackTrace();
        }
        finally{
            if(senderSocket != null){
                senderSocket.close();
            }
        }
    }
}
